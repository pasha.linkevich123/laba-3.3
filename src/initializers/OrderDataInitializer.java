package initializers;

import controller.GarageController;
import controller.MasterController;
import controller.OrderController;
import exceptions.NoRecordException;
import model.Master;

import java.util.List;

public class OrderDataInitializer implements Initializer {

    public void init() {
        GarageController.getInstance().createGarage(2, "Свободен");
        GarageController.getInstance().createGarage(5, "Свободен");
        GarageController.getInstance().createGarage(4, "Занят");
        MasterController.getInstance().createMaster("Vadim", 6);
        MasterController.getInstance().createMaster("Oleg", 7);
        OrderController.getInstance().createOrder(
                "26/04/2021",
                "30/04/2021",
                "28/04/2021",
                150,
                "Отменен",
                null,
                null
        );
        OrderController.getInstance().createOrder(
                "28/04/2021",
                "30/05/2021",
                "20/05/2021",
                100,
                "Выполнен",
                null,
                null
        );


    }

}