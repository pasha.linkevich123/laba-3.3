package initializers;

public interface Initializer {
    public void init();
}
