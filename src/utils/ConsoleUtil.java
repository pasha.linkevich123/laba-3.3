package utils;

import java.util.Scanner;

public class ConsoleUtil {

    private static Scanner scanner = new Scanner(System.in);

    public static Scanner getScanner() {
        return scanner;
    }

}
