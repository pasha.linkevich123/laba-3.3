package controller;

import api.services.MasterService;
import exceptions.NoRecordException;
import model.Master;
import services.MasterServiceImpl;

import java.util.List;

public class MasterController {

    private MasterService masterService;
    private static MasterController instance;

    private MasterController() {
        this.masterService = MasterServiceImpl.getInstance();
    }

    public static MasterController getInstance() {
        if (instance == null) {
            instance = new MasterController();
        }
        return instance;
    }

    public void createMaster(String name, int numberOrders) {
        Master master;
        master = new Master(name,numberOrders);
        createMaster(master);
    }

    public Long createMaster(Master master) {
        return masterService.create(master);
    }

    public void deleteMaster(Long id) throws NoRecordException {
        masterService.delete(id);
    }

    public Master getMaster(Long id) throws NoRecordException {
        return masterService.get(id);
    }

    public void outputMaster() throws NoRecordException {
       masterService.output();
    }
    public void  sortMaster()throws NoRecordException{
        masterService.sortMaster();
    }

}
