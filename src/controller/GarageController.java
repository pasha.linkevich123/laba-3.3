package controller;

import api.services.GarageService;
import exceptions.NoRecordException;
import model.Garage;
import services.GarageServiceImpl;

public class GarageController {
    private GarageService garageService;
    private static GarageController instance;

    private GarageController() {
        this.garageService = GarageServiceImpl.getInstance();
    }

    public static GarageController getInstance() {
        if (instance == null) {
            instance = new GarageController();
        }
        return instance;
    }

    public void createGarage(int number,String status) {
        Garage garage;
        garage = new Garage(number,status);
        createGarage(garage);

    }

    public Long createGarage(Garage garage) {
        return garageService.create(garage);
    }

    public void deleteGarage(Long id) throws NoRecordException {
        garageService.delete(id);
    }

    public Garage getGarage(Long id) throws NoRecordException {
        return garageService.get(id);
    }

    public void outputGarage() throws NoRecordException {
        garageService.output();
    }
    public void freeGarage() throws NoRecordException {
        garageService.freeGarage();
    }

}
