package controller;

import api.services.OrderService;
import exceptions.NoRecordException;
import model.Garage;
import model.Master;
import model.Order;
import services.OrderServiceImpl;

import java.util.List;

public class OrderController {

    private OrderService orderService;
    private static OrderController instance;

    private OrderController() {
        this.orderService = OrderServiceImpl.getInstance();
    }

    public static OrderController getInstance() {
        if (instance == null) {
            instance = new OrderController();
        }
        return instance;
    }

    public void createOrder(String dateSubmission,
                            String dateCompletion,
                            String plannedDateCompletion,
                            Integer price,
                            String status,
                            List<Master> masters,
                            List<Garage> garages) {

        Order order;
        order = new Order(dateSubmission,dateCompletion,plannedDateCompletion,price,status, masters,garages);
        createOrder(order);
    }

    public Long createOrder(Order order) {
        return orderService.create(order);
    }

    public void deleteOrder(Long id) throws NoRecordException {
        orderService.delete(id);
    }

    public Order getOrder(Long id) throws NoRecordException {
        return orderService.get(id);
    }

    public void outputOrder() throws NoRecordException {
        orderService.outputOrder();
    }
    public void sortOrder() throws NoRecordException {
        orderService.sortOrder();
    }
    public void displace(String dataTime)throws  NoRecordException{
        orderService.displace(dataTime);
    }

    public void sortExecuteOrder() throws NoRecordException {
        orderService.sortExecuteOrder();
    }
    public  void  intervalTimeOrder(String dataTimeOne,String dataTimeTwo)throws  NoRecordException{
        orderService.intervalTimeOrder(dataTimeOne,dataTimeTwo);
    }

    public  void addMasterToOrder(Master master)throws NoRecordException{
        orderService.addMasterToOrder(master);
    }
}