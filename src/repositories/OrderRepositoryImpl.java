package repositories;

import MemoDataStorage.DataStorage;
import api.repositories.OrderRepository;
import exceptions.NoRecordException;
import model.Master;
import model.Order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class OrderRepositoryImpl implements OrderRepository {

    private static OrderRepository instance;

    public static OrderRepository getInstance() {
        if (instance == null) {
            instance = new OrderRepositoryImpl();
        }
        return instance;
    }

    @Override
    public Long create(Order entity) {
        return DataStorage.createHero(entity).getId();
    }

    @Override
    public void delete(Long id) throws NoRecordException {
        Order orderToDelete = DataStorage.getAllOrders()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(NoRecordException::new);

        DataStorage.getAllOrders().remove(orderToDelete);
    }

    @Override
    public Optional<Order> get(Long id) {
        return DataStorage.getAllOrders()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst();
    }

    @Override
    public List<Order> getAll() {
        return DataStorage.getAllOrders();
    }


    @Override
    public void sortOrder() throws NoRecordException {
        List<Order> orderSort = DataStorage.getAllOrders();
        for (int i = 0; i < getAll().size() - 1; i++) {
            if (getAll().get(i).getDateSubmission().compareTo(getAll().get(i + 1).getDateSubmission()) > 0) {
                Collections.swap(orderSort, i, i + 1);
            } else if (getAll().get(i).getDateSubmission().compareTo(getAll().get(i + 1).getDateSubmission()) == 0) {
                if (getAll().get(i).getDateCompletion().compareTo(getAll().get(i + 1).getDateCompletion()) > 0) {
                    Collections.swap(orderSort, i, i + 1);
                } else if (getAll().get(i).getDateCompletion().compareTo(getAll().get(i + 1).getDateCompletion()) == 0) {
                    if (getAll().get(i).getPlannedDateCompletion().compareTo(getAll().get(i + 1).getPlannedDateCompletion()) > 0) {
                        Collections.swap(orderSort, i, i + 1);
                    } else if (getAll().get(i).getPlannedDateCompletion().compareTo(getAll().get(i + 1).getPlannedDateCompletion()) == 0) {
                        if (getAll().get(i).getPrice() > getAll().get(i + 1).getPrice()) {
                            Collections.swap(orderSort, i, i + 1);
                        }
                    }
                }
            }

        }
    }

    @Override
    public void outputOrder() throws NoRecordException {
        List<Order> orderOutput = DataStorage.getAllOrders();
        System.out.println(orderOutput);
    }

    @Override
    public void sortExecuteOrder() throws NoRecordException {
        List<Order> orderSort = DataStorage.getAllOrders();
        List<Order> testorderSort = new ArrayList<>();
        for (int i = 0; i < getAll().size(); i++) {
            if (getAll().get(i).getStatus() == "Выполняется") {
                testorderSort.add(orderSort.get(i));
            }
        }
        for (int i = 0; i < testorderSort.size() - 1; i++) {
            if (testorderSort.get(i).getDateSubmission().compareTo(testorderSort.get(i + 1).getDateSubmission()) > 0) {
                Collections.swap(testorderSort, i, i + 1);
            } else if (testorderSort.get(i).getDateSubmission().compareTo(getAll().get(i + 1).getDateSubmission()) == 0) {
                if (testorderSort.get(i).getDateCompletion().compareTo(testorderSort.get(i + 1).getDateCompletion()) > 0) {
                    Collections.swap(testorderSort, i, i + 1);
                } else if (testorderSort.get(i).getDateCompletion().compareTo(testorderSort.get(i + 1).getDateCompletion()) == 0) {
                    if (testorderSort.get(i).getPrice() > testorderSort.get(i + 1).getPrice()) {
                        Collections.swap(testorderSort, i, i + 1);
                    }
                }
            }
        }
        System.out.println(testorderSort);
    }

    @Override
    public void intervalTimeOrder(String dataTimeOne, String dataTimeTwo) throws NoRecordException {
        List<Order> ordersList = DataStorage.getAllOrders().stream().
                sorted(Comparator.comparing(Order::getDateSubmission).
                        thenComparing(Order::getDateCompletion).thenComparingInt(Order::getPrice)).
                collect(Collectors.toList());
        List<Order> completionList = new ArrayList<>();
        List<Order> deleteList = new ArrayList<>();
        List<Order> cancelList = new ArrayList<>();
        for (int i = 0; i < ordersList.size(); i++) {
            try {
                Date testData = new SimpleDateFormat("dd/MM/yyyy").parse(ordersList.get(i).getDateCompletion());
                Date dataOne = new SimpleDateFormat("dd/MM/yyyy").parse(dataTimeOne);
                Date dataTwo = new SimpleDateFormat("dd/MM/yyyy").parse(dataTimeTwo);
                if (dataOne.before(testData) == true && dataTwo.before(testData) != true) {
                    if (ordersList.get(i).getStatus() == "Выполнен") {
                        completionList.add(ordersList.get(i));
                    }
                    if (ordersList.get(i).getStatus() == "Отменен") {
                        cancelList.add(ordersList.get(i));
                    }
                    if (ordersList.get(i).getStatus() == "Удален") {
                        deleteList.add(ordersList.get(i));
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }

        }
        System.out.println("Выполненные заказы :");
        System.out.println(completionList);
        System.out.println("Отмененные заказы :");
        System.out.println(cancelList);
        System.out.println("Удаленные заказы :");
        System.out.println(deleteList);
    }

    @Override
    public void addMasterToOrder(Master master) throws NoRecordException {
    }

    @Override
    public void displace(String dataTime) throws NoRecordException {
        List<Order> ordersList = DataStorage.getAllOrders();

        for (int i = 0; i < ordersList.size(); i++) {
            try {
                Date testData = new SimpleDateFormat("dd/MM/yyyy").parse(ordersList.get(i).getDateCompletion());
                Date plusData = new SimpleDateFormat("dd/MM/yyyy").parse(dataTime);
                long sum=testData.getTime()+plusData.getTime();
                Date sumDate = new Date(sum);
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String data = df.format(sumDate);
                ordersList.get(i).setDateCompletion(data);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}
