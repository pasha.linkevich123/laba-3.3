package repositories;

import MemoDataStorage.DataStorage;
import api.repositories.MasterRepository;
import api.repositories.OrderRepository;
import controller.MasterController;
import exceptions.NoRecordException;
import model.Master;
import model.Order;

import java.util.*;
import java.util.stream.Collectors;


public class MasterRepositoryImpl implements MasterRepository {
    private static MasterRepository instance;

    public static MasterRepository getInstance() {
        if (instance == null) {
            instance = new MasterRepositoryImpl();
        }
        return instance;
    }


    @Override
    public Long create(Master entity) {
        return DataStorage.createMaster(entity).getId();
    }

    @Override
    public void delete(Long id) throws NoRecordException {

        Master masterToDelete = DataStorage.getAllMasters()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(NoRecordException::new);

        DataStorage.getAllMasters().remove(masterToDelete);
    }

    @Override
    public Optional<Master> get(Long id) {
        return DataStorage.getAllMasters()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst();
    }

    @Override
    public List<Master> getAll() {
        return DataStorage.getAllMasters();
    }

    @Override
    public void outputMaster() throws NoRecordException {
        List<Master> masterOutput = DataStorage.getAllMasters();
        System.out.println(masterOutput);
    }

    @Override
    public void sortMaster() throws NoRecordException {
        List<Master> masterList = DataStorage.getAllMasters().stream().
                sorted(Comparator.comparing(Master::getName).
                        thenComparingInt(Master::getNumberOrders)).
                collect(Collectors.toList());
        System.out.println(masterList);
    }
}
