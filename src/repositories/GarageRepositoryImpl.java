package repositories;

import MemoDataStorage.DataStorage;
import api.repositories.GarageRepository;
import api.repositories.MasterRepository;
import exceptions.NoRecordException;
import model.Garage;
import model.Master;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GarageRepositoryImpl implements GarageRepository {

    private static GarageRepository instance;

    public static GarageRepository getInstance() {
        if (instance == null) {
            instance = new GarageRepositoryImpl();
        }
        return instance;
    }

    @Override
    public Long create(Garage entity) {
        return DataStorage.createGarage(entity).getId();

    }

    @Override
    public void delete(Long id) throws NoRecordException {

        Garage garageToDelete = DataStorage.getAllGarages()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst()
                .orElseThrow(NoRecordException::new);

        DataStorage.getAllGarages().remove(garageToDelete);
    }

    @Override
    public Optional<Garage> get(Long id) {
        return DataStorage.getAllGarages()
                .stream()
                .filter(e -> e.getId() == id)
                .findFirst();
    }

    @Override
    public List<Garage> getAll() {
        return DataStorage.getAllGarages();
    }

    @Override
    public void outputGarage() throws NoRecordException {
        List<Garage> garagesOutput = DataStorage.getAllGarages();
        System.out.println(garagesOutput);
    }

    @Override
    public void freeGarage() throws NoRecordException {
        List<Garage> garagesOutput = DataStorage.getAllGarages();
        List<Garage> freeGaragesOutput = new ArrayList<>();
        for (int i = 0; i < garagesOutput.size(); i++) {
            if (garagesOutput.get(i).getStatus() == "Свободен")
            {
                freeGaragesOutput.add(garagesOutput.get(i));
            }
        }
        System.out.println(freeGaragesOutput);
    }
}
