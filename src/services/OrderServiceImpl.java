package services;

import api.repositories.OrderRepository;
import api.services.OrderService;
import exceptions.NoRecordException;
import model.Master;
import model.Order;
import repositories.OrderRepositoryImpl;

public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private static OrderService instance;

    private OrderServiceImpl() {
        this.orderRepository = OrderRepositoryImpl.getInstance();
    }

    public static OrderService getInstance() {
        if(instance == null) {
            instance = new OrderServiceImpl();
        }
        return instance;
    }

    @Override
    public Long create(Order entity) {
        return orderRepository.create(entity);
    }

    @Override
    public void delete(Long id) throws NoRecordException {
        orderRepository.delete(id);
    }


    @Override
    public Order get(Long id) throws NoRecordException {
        return orderRepository.get(id)
                .orElseThrow(NoRecordException::new);
    }

    @Override
    public void outputOrder() throws NoRecordException {
        orderRepository.outputOrder();
    }

    @Override
    public void sortOrder() throws NoRecordException {
        orderRepository.sortOrder();
    }

    @Override
    public void sortExecuteOrder() throws NoRecordException {
        orderRepository.sortExecuteOrder();
    }

    @Override
    public void intervalTimeOrder(String dataTimeOne,String dataTimeTwo) throws NoRecordException {
        orderRepository.intervalTimeOrder( dataTimeOne, dataTimeTwo);
    }

    @Override
    public void addMasterToOrder(Master master) throws NoRecordException {
        orderRepository.addMasterToOrder(master);
    }

    @Override
    public void displace(String dataTime) throws NoRecordException {
        orderRepository.displace(dataTime);
    }
}