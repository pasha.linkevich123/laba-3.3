package services;

import api.repositories.GarageRepository;
import api.repositories.MasterRepository;
import api.services.GarageService;
import api.services.MasterService;
import exceptions.NoRecordException;
import model.Garage;
import model.Master;
import repositories.GarageRepositoryImpl;
import repositories.MasterRepositoryImpl;

public class GarageServiceImpl implements GarageService {
    private GarageRepository garageRepository;
    private static GarageService instance;

    private GarageServiceImpl() {
        this.garageRepository = GarageRepositoryImpl.getInstance();
    }

    public static GarageService getInstance() {
        if(instance == null) {
            instance = new GarageServiceImpl();
        }
        return instance;
    }

    @Override
    public Long create(Garage entity) {
        return garageRepository.create(entity);
    }

    @Override
    public void delete(Long id) throws NoRecordException {
        garageRepository.delete(id);
    }


    @Override
    public Garage get(Long id) throws NoRecordException {
        return garageRepository.get(id)
                .orElseThrow(NoRecordException::new);
    }

    @Override
    public void output() throws NoRecordException {
        garageRepository.outputGarage();
    }

    @Override
    public void freeGarage() throws NoRecordException {
        garageRepository.freeGarage();
    }
}
