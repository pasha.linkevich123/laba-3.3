package services;

import api.repositories.MasterRepository;
import api.repositories.OrderRepository;
import api.services.MasterService;
import api.services.OrderService;
import exceptions.NoRecordException;
import model.Master;
import model.Order;
import repositories.MasterRepositoryImpl;
import repositories.OrderRepositoryImpl;

public class MasterServiceImpl implements MasterService {

    private MasterRepository masterRepository;
    private static MasterService instance;

    private MasterServiceImpl() {
        this.masterRepository = MasterRepositoryImpl.getInstance();
    }

    public static MasterService getInstance() {
        if(instance == null) {
            instance = new MasterServiceImpl();
        }
        return instance;
    }

    @Override
    public Long create(Master entity) {
        return masterRepository.create(entity);
    }

    @Override
    public void delete(Long id) throws NoRecordException {
        masterRepository.delete(id);
    }


    @Override
    public Master get(Long id) throws NoRecordException {
        return masterRepository.get(id)
                .orElseThrow(NoRecordException::new);
    }

    @Override
    public void output() throws NoRecordException {
        masterRepository.outputMaster();
    }

    @Override
    public void sortMaster() throws NoRecordException {
        masterRepository.sortMaster();
    }
}