package api.services;

import exceptions.NoRecordException;
import model.BaseEntity;
import model.Master;

public interface AbstractService <T extends BaseEntity> {

    Long create(T entity);

    void delete(Long id) throws NoRecordException;

    T get(Long id) throws NoRecordException;


}
