package api.services;

import exceptions.NoRecordException;
import model.Master;
import model.Order;

public interface MasterService extends AbstractService<Master> {
    void output() throws NoRecordException;
    void sortMaster() throws NoRecordException;
}