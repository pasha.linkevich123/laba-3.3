package api.services;

import exceptions.NoRecordException;
import model.Master;
import model.Order;

public interface OrderService extends AbstractService<Order> {
    void outputOrder() throws NoRecordException;

    void sortOrder() throws NoRecordException;

    void  sortExecuteOrder() throws NoRecordException;

    void intervalTimeOrder(String dataTimeOne,String dataTimeTwo) throws NoRecordException;

    void addMasterToOrder(Master master)throws NoRecordException;
    void displace(String dataTime)throws  NoRecordException;
}