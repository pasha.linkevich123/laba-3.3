package api.services;

import exceptions.NoRecordException;
import model.Garage;
import model.Master;

public interface GarageService extends AbstractService<Garage> {
    void output() throws NoRecordException;
    void freeGarage() throws NoRecordException;
}