package api.repositories;

import exceptions.NoRecordException;
import model.Garage;
import model.Master;

public interface GarageRepository extends AbstractRepository<Garage> {
    void outputGarage() throws NoRecordException;
    void freeGarage() throws NoRecordException;
}
