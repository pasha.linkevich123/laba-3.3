package api.repositories;

import exceptions.NoRecordException;
import model.Master;
import model.Order;

public interface OrderRepository extends AbstractRepository<Order> {
    void sortOrder() throws NoRecordException;

    void outputOrder()throws NoRecordException;

    void sortExecuteOrder()throws NoRecordException;

    void intervalTimeOrder(String dataTimeOne,String dataTimeTwo)throws NoRecordException;

    void addMasterToOrder(Master master)throws NoRecordException;

    void displace(String dataTime)throws  NoRecordException;
}
