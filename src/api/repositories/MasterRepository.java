package api.repositories;

import exceptions.NoRecordException;
import model.Master;
import model.Order;

public interface MasterRepository extends AbstractRepository<Master> {
    void outputMaster() throws NoRecordException;
    void sortMaster() throws NoRecordException;
}
