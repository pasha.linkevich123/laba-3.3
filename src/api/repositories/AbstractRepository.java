package api.repositories;

import exceptions.NoRecordException;
import model.BaseEntity;
import java.util.List;
import java.util.Optional;

public interface AbstractRepository <T extends BaseEntity> {

    Long create(T entity);

    void delete(Long entity) throws NoRecordException;

    Optional<T> get(Long id);

    List<T> getAll();



}
