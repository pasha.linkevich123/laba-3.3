package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor

public class Order extends BaseEntity {

    private String dateSubmission;
    private String dateCompletion;
    private String plannedDateCompletion;
    private Integer price;
    private String status;
    private List<Master> masters;
    private List<Garage> garages;

    public void setMasters(List<Master> masters) {
        this.masters = masters;
    }

    public void setDateCompletion(String dateCompletion) {
        this.dateCompletion = dateCompletion;
    }

    @Override
    public String toString() {
        return String.format("\nOrder:" +
                        "\r\n\tid: %d" +
                        "\r\n\tdateSubmission: %s" +
                        "\r\n\tdateCompletion: %s" +
                        "\r\n\tplannedDateCompletion: %s" +
                        "\r\n\tprice: %d" +
                        "\r\n\tstatus: %s" +
                        "\r\n\tmasters: %d" +
                        "\r\n\tgarages: %d" ,
               getId(),dateSubmission,dateCompletion,plannedDateCompletion,price,status,masters,garages);
    }
}
