package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Master extends BaseEntity {

    private String name;
    private int numberOrders;

    @Override
    public String toString() {
        return String.format("\nMaster:" +
                "\r\n\tid: %d" +
                "\r\n\tName: %s" +
                "\r\n\tnumberOrders: %d", getId(), name, numberOrders);
    }
}
