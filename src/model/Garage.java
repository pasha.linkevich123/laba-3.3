package model;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class Garage extends BaseEntity {

    private Integer number;
    private String status;

    @Override
    public String toString() {
        return String.format("\nGarage:" +
                "\r\n\tid: %d" +
                "\r\n\tnumber: %d" +
                "\r\n\tstatus: %s",getId(), number, status);
    }
}