package MemoDataStorage;

import exceptions.NoRecordException;
import model.Garage;
import model.Master;
import model.Order;
import java.util.ArrayList;
import java.util.List;

public class DataStorage {

    private static Long hero_id_sequence = 0L;
    private static List<Order> orders = new ArrayList<>();
    private static List<Master> masters = new ArrayList<>();
    private static List<Garage> garages = new ArrayList<>();
    private static Long generateOrderId() {
        return hero_id_sequence++;
    }

    public static Order createHero(Order order) {
        order.setId(generateOrderId());
        orders.add(order);
        return order;
    }
    public static Master createMaster(Master master) {
        master.setId(generateOrderId());
        masters.add(master);
        return master;
    }
    public static Garage createGarage(Garage garage) {
        garage.setId(generateOrderId());
        garages.add(garage);
        return garage;
    }
    public static List<Order> getAllOrders() {
        return orders;
    }
    public static List<Master> getAllMasters() {
        return masters;
    }
    public static List<Garage> getAllGarages() {
        return garages;
    }

}