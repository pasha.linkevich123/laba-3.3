package main;

import actions.ActionExecutor;
import actions.orders.GetOrderAction;
import initializers.Initializer;
import initializers.OrderDataInitializer;

import java.util.Arrays;
import java.util.List;

public class Runner {

    public static void main(String[] args) {

        List<Initializer> initializers = Arrays.asList(
                new OrderDataInitializer()

        );

        initializers.forEach(Initializer::init);

        ActionExecutor.execute(new GetOrderAction());
    }

}